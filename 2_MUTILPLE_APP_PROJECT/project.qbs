import qbs 1.0

Project
{
    Product {
        name: "helloworld1"
        type: "application"
        files:
            ["main.cpp"]
        Depends { name: "cpp" }
        destinationDirectory: "./project1"
    }

    Product {
        name: "helloworld2"
        type: "application"
        files:
            ["main.cpp"]
        Depends { name: "cpp" }
        destinationDirectory: "./project2"
    }
}
