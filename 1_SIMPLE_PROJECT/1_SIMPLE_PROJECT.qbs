import qbs 1.0

/*************************************************************
  All the project does the same thing
**************************************************************/
Product {
    name: "helloworld"
    type: "application"
    files:
        ["main.cpp"]
    Depends { name: "cpp" }
    destinationDirectory: {
        console.info( "====================>>>>>>>>>>>>>>>>>>>>>>>>>>>WELCOME_TO_QBS_BUILD_SYSTEM<<<<<<<<<<<<<<<<<<<<<<==================================" )
        return "./customeDir"
    }
}

//CppApplication {
//    name: "helloworld"
//    files: "main.cpp"
//}

//Application {
//    name: "helloworld"
//    files: "main.cpp"
//    Depends { name: "cpp" }
//}
